/* ----------------
  WHEEL FUNCTIONS
--------------- */

var Wheel = {
  BASE_DEGREE: null,
  currentDegree: null,
  set: null,
  index: 0,

  /*
    Initiate constant and other stuff
      @param loc = The first location object
  */
  init: function(loc) {
    Wheel.BASE_DEGREE = Math.round(Raphael.angle(loc.text.attr("x"), loc.text.attr("y"), setting.CENTER, setting.CENTER) );
    Wheel.rotateTop(loc); // rotate to itself, disable weird animation
  },

  /*
    Draw the wheel
      @param paper = Raphael paper
             path = path for the text to be drawn
      @return all Location object
  */
  draw: function(paper, path) {
    var all = [];

    // Initiate all graphs
    paper.setStart();
    for(var i = 0, len = setting.DATA_LENGTH; i < len; i++) {
      var loc = new Location(data.locations[i], paper, path);
      loc.init();
      loc.draw(i);

      all.push(loc);
    }
    Wheel.set = paper.setFinish();

    return all;
  },

  /*
    Draw path for Wheel text
      @param paper = Raphael paper
      @return path for the text
  */
  drawPath: function(paper) {
    var radius = setting.GRAPH / 2;
    var pathRadius = radius + setting.TEXT_GUTTER;

    var canvasGutter = (setting.CANVAS - setting.GRAPH) / 2;
    var startX = setting.CENTER;
    var startY = canvasGutter - setting.TEXT_GUTTER;

    var startingPath = ["M", startX, startY];
    var arcPath = ["a", pathRadius, pathRadius, 0, 1, 1, -1, 0.001, "z"];

    var path = paper.path(startingPath.concat(arcPath) );
    path.attr("stroke", "transparent");

    return path;
  },

  /*
    Spin all texts so it become readable from left-to-right
  */
  adapt: function() {
    var list = getAnchorEndList(Wheel.index);

    Wheel.set.forEach(function(e) {
      var isAnchorEnd = false;
      for(var i = 0, len = list.length; i < len; i++) {
        if(list[i] === e.data("index") ) {
          isAnchorEnd = true;
          break;
        }
      }

      // If should be anchored end but currently anchored start
      if(isAnchorEnd && e.attr("text-anchor") === "start") {
        e.attr("text-anchor", "end");
      }
      else if(!isAnchorEnd && e.attr("text-anchor") === "end") {
        e.attr("text-anchor", "start");
      }
    });
  },

  /*
    Rotate the selected element to top
      @param index = position of the selected text
  */
  rotateTop: function(loc, animationTime) {
    setting.$CANVAS.addClass("is-spinning");

    Wheel.index = loc.index;

    // Calculate degree for wheel
    var selectedDegree = Math.round(Raphael.angle(loc.text.attr("x"), loc.text.attr("y"), setting.CENTER, setting.CENTER) );
    var wheelDegree = Wheel.BASE_DEGREE - selectedDegree;

    // Update global variable
    Wheel.currentDegree = wheelDegree;

    // Rotate the wheel set
    Wheel.set.forEach(function(e) {
      // Calculate degree for text
      var textDegree = Math.round( Raphael.angle(e.attr("x"), e.attr("y"), setting.CENTER, setting.CENTER) );

      if(e.attr("text-anchor") === "end") {
        textDegree = textDegree - 180;
      }

      e.animate({
        transform: ["r", wheelDegree, setting.CENTER, setting.CENTER, "r", textDegree, e.attr("x"), e.attr("y")],
      }, animationTime);
    });

    setTimeout(function(){
      setting.$CANVAS.removeClass("is-spinning");
    }, animationTime);
  },

  // Clear the active state on all nodes
  clearActiveNode: function() {
    setting.$CANVAS.find(".active").removeClass("active");
  },
};
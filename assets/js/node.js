/* ---------------
  NODE Object
--------------- */

function Node(data, graph) {
  this.data = data;
  this.graph = graph;
}

Node.prototype = {

  draw: function(index) {
    var radius = setting.NODE / 2;

    // Calculate position
    var point = this.graph.nodesPlacement[index];

    // Draw
    var color  = rgb2hex(palette[this.data.type]);
    var colorDark = rgb2hex(palette[this.data.type + "Dark"]);
    var circle = this.graph.paper.circle(point.x, point.y, radius);

    circle.attr({
      fill: "45-" + color + ":5-" + colorDark + ":40-" + color + ":100",
      stroke: "transparent",
    })
    .innerShadow(1, -2, 0, "black", 0.5)
    .data({
      name: this.data.name,
      city: this.data.city,
      image: this.data.image,
      year: this.data.year,
      type: this.data.type,
      platform: this.data.platform,
      projects: this.data.projects,
      is_group: this.data.is_group,
    })
    .click( function() {
      // setting active state
      setting.$CANVAS.find(".active").removeClass("active");
      this.node.setAttribute("class", "active node");
      // popup the project details
      Sidebar.popup(this.data() );
    });

    circle.node.setAttribute("class", "node");
  },
};
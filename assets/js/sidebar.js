/* ------------------
  SIDEBAR FUNCTIONS
------------------- */

var Sidebar = {

  /*
    Update the Project Count in Left sidebar
  */
  update: function(node, location) {
    var $sidebar = $(".sidebar");
    var locName = location.full_name || location.name;
    $(".selected-city").text(locName);

    var categoryCount = {
      public: 0,
      private: 0,
      mapletree: 0,
      total: 0,
    };

    for(var i = 0, len = location.projects.length; i < len; i++) {
      var project = location.projects[i];
      categoryCount[project.category] += 1;
      categoryCount["total"] += 1;
    }

    // update individual category
    // for(var c in categoryCount) {
    //   $("#" + c + "-count").text(categoryCount[c]);
    // }

    /* Update by ARUL on 08/08/2014 */
    $("#public-count").text(location.public_reits);
    $("#private-count").text(location.private_funds);
    $("#mapletree-count").text(location.mapletree_owned);
    $("#all-count").text(location.total_projects);


    // update the Total Projects
    //$("#all-count").text(categoryCount["total"]);

    // add "selected" class
    $(".selected").removeClass("selected");
    node.setAttribute("class", "selected");

    // animate
    $(".sidebar header, .sidebar article").addClass("animated fast fadein-left");
    setTimeout(function() {
      $(".sidebar header, .sidebar article").removeClass("animated fast fadein-left");
    }, 200);
  },

  /*
    Popup the Project Detail in Right sidebar
  */
  popup: function(data) {
    $("body").addClass("popup-opened");

    var raw, $container;
    if(data.is_group) {
      Sidebar.clearPopup();
      raw = $("#group").html();
      $container = $(".group");

      if(data.projects.length > 7) {
        data["has_arrow"] = true;
      }
    }
    else if(data.is_from_group) {
      raw = $("#project").html();
      $container = $(".group-project");
    }
    else {
      Sidebar.clearPopup();
      raw = $("#project").html();
      $container = $(".single-project");
    }

    var template = Handlebars.compile(raw);
    var html = template(data);
    $container.html(html).addClass("animated fast fadein-right");

    setTimeout(function() {
      $container.removeClass("animated fast fadein-right");
    }, 200);
  },

  clearPopup: function() {
    $(".popup").html("");
  },

  closeGroup: function() {
    $(".group, .group-project").html("");
    Wheel.clearActiveNode();
  },

  closeProject: function() {
    $(".single-project, .group-project").html("");
    Wheel.clearActiveNode();
  },

  scrollGroup: function($container, value) {
    var position = $container.scrollTop() + value;
    $container.scrollTop(position);
  },
}

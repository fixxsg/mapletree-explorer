/* -----------------
  LOCATION Object
----------------- */

function Location(data, paper, path) {
  this.data = data;
  this.paper = paper;

  this.path = path; // the wheel path
  this.index; // the position in the wheel

  this.graphs = {
    public: { exist: false },
    private: { exist: false },
    mapletree: { exist: false }
  };

  this.text; // the <text> element
  this.textDegree = 0; // the rotation degree of the <text> element
  this.set; // the graph + nodes set
}

Location.prototype = {

  init: function() {
    var graphSizes  = [
      {
        name: "large",
        num: setting.GRAPH
      },
      {
        name: "medium",
        num: setting.GRAPH_MID,
      },
      {
        name: "small",
        num: setting.GRAPH_INNER
      }
    ];

    // Check whether a location has that category
    for(var i = 0, len = this.data.projects.length; i < len; i++) {  
      switch(this.data.projects[i].category) {
        case "public":
          this.graphs.public.exist = true;
          break;
        case "private":
          this.graphs.private.exist = true;
          break;
        case "mapletree":
          this.graphs.mapletree.exist = true;
          break;
      }
    }

    // Initiate the graph and delete missing one
    for(var g in this.graphs) {
      if(this.graphs.hasOwnProperty(g) ) {
        // If the location has project in this category
        if(this.graphs[g].exist) {
          var size = graphSizes.shift();
          this.graphs[g] = new Graph(this.paper, size);
        } else {
          delete this.graphs[g];
        }
      }
    }
  },

  /*
    Draw the Location text
      @param index = index position of the text
  */
  draw: function(index) {
    this.index = index;

    var pointLength = (this.path.getTotalLength() / setting.DATA_LENGTH) * index;
    var point = this.path.getPointAtLength(pointLength);

    this.text = this.paper.text(point.x, point.y, this.data.name);

    // rotate the text
    var degree = Math.round(Raphael.angle(setting.CENTER, setting.CENTER, point.x, point.y) );
    this.textDegree = degree;

    if(degree > 271 || degree < 75) {
      this.text.transform(["r", degree])
          .attr("text-anchor", "end");
    } else {
      this.text.transform(["r", degree - 180])
          .attr("text-anchor", "start");
    }

    var _this = this;
    this.text.data({
      name: this.data.name,
      index: this.index,
    })
    .click(function() {
      _this.onclick();
    });
  },

  onclick: function() {
    $(".popup").html("");
    this.removeGraph();
    this.drawGraph();

    var _this = this;
    var animationTime = 500;
      
    // If not mobile
    if(!agent.mobile) {
      Wheel.rotateTop(_this, Math.abs(animationTime) );

      // Adapt the text so all is readable
      setTimeout(function() {
        Wheel.adapt();
        Wheel.rotateTop(_this, 0); // rotate to itself, fix weird bug
      }, animationTime + 10);
    }
    // if mobile, show the highlight
    else {
      var $highlight = $(".highlight");
      var x = this.text.attr("x");
      var y = this.text.attr("y");

      $(".highlight").css({
        top: y - $highlight.height(),
        left: x - ($highlight.width() / 2),
        transform: "rotate(" + (this.textDegree - 90) + "deg)"
      });
    }
  },

  /*
    Draw the graph
  */
  drawGraph: function() {
    var groups = {};

    this.paper.setStart();
    
    // draw the graph
    for(var g in this.graphs) {
      if(this.graphs.hasOwnProperty(g) ) {    
        this.graphs[g].draw(palette[g], palette[g + "Dark"]);
      }
    }

    // initiate group
    if(this.data.groups) {
      for(var i = 0, len = this.data.groups.length; i < len; i++) {
        var group = this.data.groups[i];
        group["is_group"] = true;
        group["projects"] = [];
        groups[group.id] = group;
      }
    }

    // draw the nodes
    for(var i = 0, len = this.data.projects.length; i < len; i++) {
      var project = this.data.projects[i];
      project["is_group"] = false;

      // if project is inside group, collect it
      if(project.group_id) {
        groups[project.group_id]["projects"].push(project);
      }
      // else draw it
      else {
        this.graphs[project.category].insert(project);
      }
    }

    // draw the group
    if(this.data.groups) {
      for(var g in groups) {
        if(groups.hasOwnProperty(g) ) {
          var group = groups[g];
          this.graphs[group.category].insert(group);
        }
      }
    }

    this.set = this.paper.setFinish();

    // update sidebar
    Sidebar.update(this.text.node, this.data);
  },

  /*
    Remove the graph
  */
  removeGraph: function() {
    if(this.set !== undefined) {
      this.set.remove();
    }
  },
};
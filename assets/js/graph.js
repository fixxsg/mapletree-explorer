/* ---------------
  GRAPH
--------------- */

function Graph(paper, size) {
  this.paper = paper;

  this.size = size;
  this.path; // path to draw the node;

  this.nodesCount = 0;

  this.nodesPlacement = [];
}

Graph.prototype = {
  draw: function(color, colorDark) {
    this.nodesCount = 0; // reset the count

    color = rgb2hex(color);
    colorDark = rgb2hex(colorDark);
    var radius = this.size.num / 2;

    // draw big circle
    var centerY = radius + ((setting.CANVAS - setting.GRAPH) / 2)
    var circle = this.paper.circle(setting.CENTER, centerY, radius);
    circle.attr("fill", "45-" + color + ":5-" + colorDark + ":40-" + color + ":100");
    circle.attr("stroke", "transparent");

    this.drawPath(radius, centerY);
  },

  /*
    Draw path for Nodes placement
  */
  drawPath: function(radius, centerY) {
    // draw invis path for nodes
    var outerRadius = radius - setting.GUTTER;
    var outerStart = ["M", (radius + setting.CENTER - setting.GUTTER), centerY];
    var outerArc = ["a", outerRadius, outerRadius, 0, 0, 1, -(outerRadius * 2), 0];

    var outerPath = outerStart.concat(outerArc);

    var innerRadius = outerRadius - setting.NODE;
    var innerStart = ["m", setting.NODE, 2];
    var innerArc = ["a", innerRadius, innerRadius, 0, 1, 0, (innerRadius * 2), 0];

    var innerPath = innerStart.concat(innerArc);

    this.path = this.paper.path(outerPath.concat(innerPath) );
    this.path.attr("stroke", "transparent");

    this.calcPlacement();
  },

  /*
    Calculate the coordinate for nodes placement
    - Indexes is manually counted
    - TODO: make this dynamic if possible
  */
  calcPlacement: function() {
    var indexes;
    switch(this.size.name) {
      case "large":
        indexes = [7, 8, 6, 17, 18, 9, 5, 16, 4, 19];
        break;
      case "medium":
        indexes = [5, 6, 4, 11, 12, 7, 3, 10];
        break;
      case "small":
        indexes = [3, 4, 2];
        break;
    }

    for(var i = 0, len = indexes.length; i < len; i++) {
      var pathLength = (indexes[i] - 1) * setting.NODE;
      var point = this.path.getPointAtLength(pathLength);
      this.nodesPlacement.push(point);
    }
  },

  /*
    Insert new node into the graph
  */
  insert: function(data) {
    var n = new Node(data, this);
    this.nodesCount += 1;

    n.draw(this.nodesCount - 1);
  },

};
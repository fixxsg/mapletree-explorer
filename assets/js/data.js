var data = { locations: [
  // Singapore
  {
    name: "Singapore",
    projects: [
      // Single
      {
        id: 1,
        name: "The Beacon",
        city: "Singapore",
        image: "singapore/the-beacon.png",
        year: 2008,
        category: "mapletree",
        type: "residential",
        platform: "Mapletree Investments",
      },
      {
        id: 2,
        name: "Bank of America Merrill Lynch HarbourFront",
        city: "Singapore",
        image: "singapore/bank-america.png",
        year: 2008,
        category: "public",
        type: "office",
        platform: "Mapletree Commercial Trust",
      },
      {
        id: 3,
        name: "HarbourFront Tower One",
        city: "Singapore",
        image: "singapore/harbourfront-tower-one.png",
        year: 2002,
        category: "mapletree",
        type: "office",
        platform: "Mapletree Investments",
      },
      {
        id: 4,
        name: "HarbourFront Tower Two",
        city: "Singapore",
        image: "singapore/harbourfront-tower-two.png",
        year: 2003,
        category: "mapletree",
        type: "office",
        platform: "Mapletree Investments",
      },
      {
        id: 5,
        name: "Mapletree Lighthouse",
        city: "Singapore",
        image: "singapore/mapletree-lighthouse.png",
        year: null,
        category: "mapletree",
        type: "office",
        platform: "Mapletree Investments",
      },
      {
        id: 6,
        name: "St James Power Station",
        city: "Singapore",
        image: "singapore/stjames-power.png",
        year: 1927,
        category: "mapletree",
        type: "commercial",
        platform: "Mapletree Investments",
      },
      {
        id: 7,
        name: "Mapletree Business City",
        city: "Singapore",
        image: "singapore/mapletree-business.png",
        year: 2010,
        category: "mapletree",
        type: "commercial",
        platform: "Mapletree Investments",
      },
      {
        id: 8,
        name: "The Comtech",
        city: "Singapore",
        image: "singapore/the-comtech.png",
        year: 2003,
        category: "mapletree",
        type: "commercial",
        platform: "Mapletree Investments",
      },
      {
        id: 9,
        name: "Mapletree Anson",
        city: "Singapore",
        image: "singapore/mapletree-anson.png",
        year: 2009,
        category: "public",
        type: "office",
        platform: "Mapletree Commercial Trust",
      },
      {
        id: 10,
        name: "HarbourFront Centre",
        city: "Singapore",
        image: "singapore/harbourfront-centre.png",
        year: 1978,
        category: "mapletree",
        type: "commercial",
        platform: "Mapletree Investments",
      },
      {
        id: 11,
        name: "VivoCity",
        city: "Singapore",
        image: "singapore/vivocity.png",
        year: 2006,
        category: "public",
        type: "commercial",
        platform: "Mapletree Commercial Trust",
      },
      {
        id: 12,
        name: "PSA Building",
        city: "Singapore",
        image: "singapore/psa-building.png",
        year: 2085,
        category: "public",
        type: "commercial",
        platform: "Mapletree Commercial Trust",
      },
      // Industrial Group
      {
        id: 13,
        name: "The Synergy",
        city: "Singapore",
        image: "singapore/the-synergy.png",
        year: 1998,
        category: "public",
        type: "industrial",
        platform: "Mapletree Industrial Trust",
        group_id: 1,
      },
      {
        id: 14,
        name: "The Strategy",
        city: "Singapore",
        image: "singapore/the-strategy.png",
        year: 2002,
        category: "public",
        type: "industrial",
        platform: "Mapletree Industrial Trust",
        group_id: 1,
      },
      {
        id: 15,
        name: "The Signature",
        city: "Singapore",
        image: "singapore/the-signature.png",
        year: 2005,
        category: "public",
        type: "industrial",
        platform: "Mapletree Industrial Trust",
        group_id: 1,
      },
      // Logistic Group
      {
        id: 16,
        name: "Toppan",
        city: "Singapore",
        image: "singapore/toppan.png",
        year: 2005,
        category: "public",
        type: "logistic",
        platform: "Mapletree Logistcs Trust",
        group_id: 2,
      },
      {
        id: 17,
        name: "Natural Cool Lifestyle Hub",
        city: "Singapore",
        image: "singapore/natural-cool.png",
        year: 2010,
        category: "public",
        type: "logistic",
        platform: "Mapletree Logistics Trust",
        group_id: 2,
      },
      {
        id: 18,
        name: "Tang Logistics Centre",
        city: "Singapore",
        image: "singapore/tang-logistics.png",
        year: 2006,
        category: "public",
        type: "logistic",
        platform: "Mapletree Logistics Trust",
        group_id: 2,
      },
      {
        id: 19,
        name: "Kingsmen Creatives",
        city: "Singapore",
        image: "singapore/kingsmen-creatives.png",
        year: 2007,
        category: "public",
        type: "logistic",
        platform: "Mapletree Logistics Trust",
        group_id: 2,
      },
      {
        id: 20,
        name: "CEVA (Changi South)",
        city: "Singapore",
        image: "singapore/ceva.png",
        year: 2010,
        category: "public",
        type: "logistic",
        platform: "Mapletree Logistics Trust",
        group_id: 2,
      },
      {
        id: 21,
        name: "8 Changi South Lane",
        city: "Singapore",
        image: "singapore/8-changi.png",
        year: 2006,
        category: "public",
        type: "logistic",
        platform: "Mapletree Logistics Trust",
        group_id: 2,
      },
      {
        id: 22,
        name: "70 Alps Avenue",
        city: "Singapore",
        image: "singapore/70-alps.png",
        year: 2005,
        category: "public",
        type: "logistic",
        platform: "Mapletree Logistics Trust",
        group_id: 2,
      },
      {
        id: 23,
        name: "6 Changi South Lane",
        city: "Singapore",
        image: "singapore/6-changi.png",
        year: 2005,
        category: "public",
        type: "logistic",
        platform: "Mapletree Logistics Trust",
        group_id: 2,
      },
      {
        id: 24,
        name: "Pulau Sebarok",
        city: "Singapore",
        image: "singapore/pulau-sebarok.png",
        year: 2005,
        category: "public",
        type: "logistic",
        platform: "Mapletree Logistics Trust",
        group_id: 2,
      },
      {
        id: 25,
        name: "Menlo (Boon Lay Way)",
        city: "Singapore",
        image: "singapore/menlo.png",
        year: 2008,
        category: "public",
        type: "logistic",
        platform: "Mapletree Logistics Trust",
        group_id: 2,
      },
      {
        id: 26,
        name: "Armstrong",
        city: "Singapore",
        image: "singapore/armstrong.png",
        year: 2005,
        category: "public",
        type: "logistic",
        platform: "Mapletree Logistics Trust",
        group_id: 2,
      },
      {
        id: 27,
        name: "Jurong Logistics Hub",
        city: "Singapore",
        image: "singapore/jurong-logistics.png",
        year: 2006,
        category: "public",
        type: "logistic",
        platform: "Mapletree Logistics Trust",
        group_id: 2,
      },
      {
        id: 28,
        name: "TIC Tech Centre",
        city: "Singapore",
        image: "singapore/tic-tech.png",
        year: 2004,
        category: "public",
        type: "logistic",
        platform: "Mapletree Logistics Trust",
        group_id: 2,
      },
      {
        id: 29,
        name: "Markono",
        city: "Singapore",
        image: "singapore/markono.png",
        year: 2006,
        category: "public",
        type: "logistic",
        platform: "Mapletree Logistics Trust",
        group_id: 2,
      },
      {
        id: 30,
        name: "Mapletree Benoi Logistics Hub",
        city: "Singapore",
        image: "singapore/mapletree-benoi.png",
        year: null,
        category: "public",
        type: "logistic",
        platform: "Mapletree Logistics Trust",
        group_id: 2,
      },
    ],
    groups: [
      {
        id: 1,
        name: "Industrial - Projects",
        category: "public",
        type: "industrial",
      },
      {
        id: 2,
        name: "Logistics - Projects",
        category: "public",
        type: "logistic",
      },
    ],
  },
  // Malaysia
  {
    name: "Malaysia",
    projects: [
      {
        name: "Mapletree Shah Alam Logistics Park",
        city: "Selangor Darul Ehsan",
        image: "malaysia/shah-alam.png",
        year: 2009,
        type: "logistic",
        category: "mapletree",
        platform: "Mapletree Investments",
        group_id: 3,
      },
      {
        name: "Seri Tanjung Pinang Villas",
        city: "Penang",
        image: "malaysia/seri-tanjung.png",
        year: null,
        category: "private",
        type: "residential",
        platform: "CIMB Mapletree Real Estate Fund 1",
      },
      {
        name: "Papillon",
        city: "Kuala Lumpur",
        image: "malaysia/papillon.png",
        year: 2011,
        category: "private",
        type: "residential",
        platform: "CIMB Mapletree Real Estate Fund 1",
      },
      {
        name: "Jaya Shopping Centre",
        city: "Kuala Lumpur",
        image: "malaysia/jaya-shopping.png",
        year: null,
        category: "private",
        type: "commercial",
        platform: "CIMB Mapletree Real Estate Fund 1",
      },
      {
        name: "Papillon",
        city: "Kuala Lumpur",
        image: "malaysia/papillon.png",
        year: 2011,
        category: "private",
        type: "residential",
        platform: "CIMB Mapletree Real Estate Fund 1",
      },
      {
        name: "Giant Prima Prai",
        city: "Penang",
        image: "malaysia/giant-prima.png",
        year: 2011,
        category: "private",
        type: "commercial",
        platform: "CIMB Mapletree Real Estate Fund 1",
      },
      {
        name: "Ceriaan Kiara",
        city: "Kuala Lumpur",
        image: "malaysia/ceriaan-kiara.png",
        year: 2010,
        category: "private",
        type: "residential",
        platform: "CIMB Mapletree Real Estate Fund 1",
      },
      {
        name: "MENARA CIMB",
        city: "Kuala Lumpur",
        image: "malaysia/menara-cmb.png",
        year: null,
        category: "private",
        type: "office",
        platform: "CIMB Mapletree Real Estate Fund 1",
      },
      {
        name: "Chee Wah",
        city: "Puchong",
        image: "malaysia/chee-wah.jpg",
        year: 2007,
        category: "public",
        type: "logistic",
        platform: "Mapletree Logistics Trust",
        group_id: 4,
      },
      {
        name: "Linfox",
        city: "Shah Alam",
        image: "malaysia/linfox.png",
        year: 2007,
        category: "public",
        type: "logistic",
        platform: "Mapletree Logistics Trust",
        group_id: 4,
      },
      {
        name: "Pancuran",
        city: "Shah Alam",
        image: "malaysia/pancuran.png",
        year: 2006,
        category: "public",
        type: "logistic",
        platform: "Mapletree Logistics Trust",
        group_id: 4,
      },
    ],
    groups: [
      {
        id: 3,
        name: "Logistics - Projects",
        category: "mapletree",
        type: "logistic",
      },
      {
        id: 4,
        name: "Logistics - Projects",
        category: "public",
        type: "logistic",
      }
    ],
  },
  {
    name: "Vietnam",
    projects: [
      {
        name: "",
        city: "",
        image: "",
        year: 2000,
        category: "mapletree",
        type: "office",
        platform: "",
      },
    ],
  },
  {
    name: "India",
    projects: [
      {
        name: "",
        city: "",
        image: "",
        year: 2000,
        category: "mapletree",
        type: "office",
        platform: "",
      },
    ],
  },
  {
    name: "Beijing",
    projects: [
      {
        name: "",
        city: "",
        image: "",
        year: 2000,
        category: "mapletree",
        type: "office",
        platform: "",
      },
    ],
  },
  // Shanghai
  {
    name: "Shanghai",
    projects: [
      {
        name: "Northwest Logistics Park",
        city: "Shanghai",
        image: "shanghai/northwest-logistics.png",
        year: 2008,
        category: "public",
        type: "logistic",
        platform: "Mapletree Logistics Trust",
        group_id: "11",
      },
      {
        name: "Ouluo Logistics Centre",
        city: "Shanghai",
        image: "shanghai/ouluo-logistics.png",
        year: 2006,
        category: "public",
        type: "logistic",
        platform: "Mapletree Logistics Trust",
        group_id: "11",
      },
      {
        name: "Silver Court",
        city: "Shanghai",
        image: "shanghai/silver-court.png",
        year: 2003,
        category: "private",
        type: "commercial",
        platform: "Mapletree India China (MIC) Fund",
      },
      {
        name: "Mapletree Baoshan Industrial Park",
        city: "Shanghai",
        image: "shanghai/baoshan-industrial.png",
        year: 2008,
        category: "private",
        type: "industrial",
        platform: "Mapletree Industrial Fund",
        group_id: "12",
      },
      {
        name: "Project Mercury-Minhang",
        city: "Shanghai",
        image: "shanghai/project-mercury.png",
        year: null,
        category: "private",
        type: "commercial",
        platform: "Mapletree India China (MIC) Fund",
      },
      {
        name: "Mapletree Yangshan Bonded Logistics Park",
        city: "Shanghai",
        image: "shanghai/yangshan-bonded.png",
        year: 2006,
        category: "mapletree",
        type: "logistic",
        platform: "Mapletree Logistics Trust",
        group_id: "13",
      },
    ],
    groups: [
      {
        id: 11,
        name: "Logistics - Projects",
        category: "public",
        type: "logistic",
      },
      {
        id: 12,
        name: "Industrial - Projects",
        category: "private",
        type: "industrial",
      },
      {
        id: 13,
        name: "Logistics - Projects",
        category: "mapletree",
        type: "logistic",
      },
    ],
  },
  {
    name: "Zhengzhou",
    full_name: "Zhengzhou, Henan",
    projects: [
      {
        name: "",
        city: "",
        image: "",
        year: 2000,
        category: "mapletree",
        type: "office",
        platform: "",
      },
    ],
  },
  {
    name: "Foshan",
    full_name: "Foshan, China",
    projects: [
      {
        name: "",
        city: "",
        image: "",
        year: 2000,
        category: "mapletree",
        type: "office",
        platform: "",
      },
    ],
  },
  {
    name: "Xi'an",
    full_name: "Xi'an, China",
    projects: [
      {
        name: "",
        city: "",
        image: "",
        year: 2000,
        category: "mapletree",
        type: "office",
        platform: "",
      },
    ],
  },
  {
    name: "Wuxi",
    full_name: null,
    projects: [
      {
        name: "",
        city: "",
        image: "",
        year: 2000,
        category: "mapletree",
        type: "office",
        platform: "",
      },
    ],
  },
  {
    name: "Tianjin",
    full_name: "",
    projects: [
      {
        name: "",
        city: "",
        image: "",
        year: 2000,
        category: "mapletree",
        type: "office",
        platform: "",
      },
    ],
  },
  {
    name: "Guangzhou",
    projects: [
      {
        name: "",
        city: "",
        image: "",
        year: 2000,
        category: "mapletree",
        type: "office",
        platform: "",
      },
    ],
  },
  {
    name: "Hong Kong",
    projects: [
      {
        name: "",
        city: "",
        image: "",
        year: 2000,
        category: "mapletree",
        type: "office",
        platform: "",
      },
    ],
  },
  {
    name: "Japan",
    projects: [
      {
        name: "",
        city: "",
        image: "",
        year: 2000,
        category: "mapletree",
        type: "office",
        platform: "",
      },
    ],
  },
  {
    name: "Korea",
    projects: [
      {
        name: "",
        city: "",
        image: "",
        year: 2000,
        category: "mapletree",
        type: "office",
        platform: "",
      },
    ],
  },
] };

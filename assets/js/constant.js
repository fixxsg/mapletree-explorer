/* -------------
  CONSTANT
------------- */

// Color Palette, taken from hidden element
var palette = {
  public: $(".public-circle.light").css("background-color"),
  private: $(".private-circle.light").css("background-color"),
  mapletree: $(".mapletree-circle.light").css("background-color"),

  publicDark: $(".public-circle.dark").css("background-color"),
  privateDark: $(".private-circle.dark").css("background-color"),
  mapletreeDark: $(".mapletree-circle.dark").css("background-color"),

  commercial: $(".commercial-circle.light").css("background-color"),
  office: $(".office-circle.light").css("background-color"),
  industrial: $(".industrial-circle.light").css("background-color"),
  logistic: $(".logistic-circle.light").css("background-color"),
  residential: $(".residential-circle.light").css("background-color"),

  commercialDark: $(".commercial-circle.dark").css("background-color"),
  officeDark: $(".office-circle.dark").css("background-color"),
  industrialDark: $(".industrial-circle.dark").css("background-color"),
  logisticDark: $(".logistic-circle.dark").css("background-color"),
  residentialDark: $(".residential-circle.dark").css("background-color"),
};

// Base Setting
var setting = {
  $CANVAS: $(".canvas"),
  CANVAS: 830,
  GRAPH: 600, // locked, don't change
  NODE: 70, // locked dont' change
  TEXT_GUTTER: 10, // locked dont'change
  DATA_LENGTH: data.locations.length,
};

// set the inner graph size
setting.GUTTER = setting.NODE / 2;
setting.GRAPH_MID = setting.GRAPH - (setting.NODE * 2 + setting.GUTTER);
setting.GRAPH_INNER = setting.GRAPH_MID - (setting.NODE * 2 + setting.GUTTER);

setting.CENTER = setting.CANVAS / 2;

// Change canvas size
setting.$CANVAS.width(setting.CANVAS);
setting.$CANVAS.height(setting.CANVAS);
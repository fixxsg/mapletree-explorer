/* ------------------
  GENERIC FUNCTIONS   
------------------- */

function getAnchorEndList(currentIndex) {
  var list = [];
  var halfLength = Math.floor((setting.DATA_LENGTH - 1) / 2);

  for(var i = 1; i <= halfLength; i++) {
    var index = (currentIndex - i) % setting.DATA_LENGTH;
    if(index < 0) {
      index = setting.DATA_LENGTH + index;
    }
    list.push(index);
  }
  return list;
}

/* Convert RGB to Hex value */
function rgb2hex(rgb) {
  rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
  function hex(x) {
    return ("0" + parseInt(x).toString(16) ).slice(-2);
  }
  return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

function init() {
  // Initiate Raphael Paper
  var canvas = setting.$CANVAS.get(0);
  var paper = Raphael(canvas, setting.CANVAS, setting.CANVAS);

  // Draw text and wheel
  var path = Wheel.drawPath(paper);
  var locations = Wheel.draw(paper, path);

  // Draw first graph
  locations[0].drawGraph();

  Wheel.init(locations[0]);
}


/* -------
  START   
------- */

$(document).ready(function() {
  init();

  // Click project in group list
  $(".group").on("click", "a", function() {
    var $this = $(this);
    var data = {
      name: $this.data("name"),
      city: $this.data("city"),
      image: $this.data("image"),
      year: $this.data("year"),
      platform: $this.data("platform"),
      is_group: false,
      is_from_group: true,
    };

    Sidebar.popup(data);
  });

  $(".group").on("click", "header", function() {
    var $arrow = $(this);
    var $list = $arrow.siblings("article").find("ul");
    Sidebar.scrollGroup($list, -35);
  });

  $(".group").on("click", "footer", function() {
    var $arrow = $(this);
    var $list = $arrow.siblings("article").find("ul");
    Sidebar.scrollGroup($list, 35);
  });
});

// User Agent Test and MODERNIZR
var agent = {
  mobile: (navigator.userAgent.match(/(iPad|iPhone|iPod|Android|Blackberry)/g) ? true : false)
};

Modernizr.addTest("mobile", function(){
  return agent.mobile;
});


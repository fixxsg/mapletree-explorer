MAPLETREE
===============

Write introduction here Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste porro nam eum nobis dolorum enim reprehenderit at totam minus dolor?

Dependencies
===================

JAVASCRIPT

* jQuery 10.1

* [jQuery SVG](http://keith-wood.name/svg.html) - Allow jQuery to interact with SVG elements

* [Modernizr](http://modernizr.com/) - Detect browser support

* [Raphael](http://raphaeljs.com/) - Draw vector graphic

* [Fastclick](http://ftlabs.github.io/fastclick/) - Remove 0.3s touch delay

* [Handlebars](http://handlebarsjs.com/) - Templating engine

STYLESHEET

* [Sass](http://sass-lang.com/) - CSS on steroid

* [Compass](http://compass-style.org/) - Sass on steroid

* [Edge](https://github.com/HennerS/Edge) - Compass on steroid

To compile the Sass, open `cmd` in the project directory and type:

    compass watch

Terminology
=============

**Graph** is the big circle that represents category.

**Node** is the small circle that represents project.

**Text** is the country or city name that circles the graph to form the sun. We also call it **Location**.

Sample JSON
============

    {
      locations: [
        {
          id: 1,
          name: "Singapore",
          groups: [
            {
              id: 100,
              name: "Industrial - Projects",
              category: "public",
              type: "industrial",
              location_id: 1,
            },
            { ... },
          ],
          projects: [
            {
              id: 10,
              name: "Hoot Kiam Apartment",
              image: "http://s3.amazonaws.com/mapletree/Sf8dfds0.jpg",
              type: "commercial",
              year: 2007,
              category: "public",
              platform: "Mapletree Investment",
              location_id: 1,
            },
            {
              id: 11,
              name: "Vivo City",
              ...
              group_id: 100,
            },
            { ... },
          ]
        },

        {
          id: 2,
          name: "Indonesia",
          projects: [ ... ],
        }
      ]
    }

Field Explanation
--------------------

**CATEGORY**

Category handles the placing on graph. There are 3 possible categories:

* public
* private
* mapletree

**TYPE**

Type handles the coloring of node. There are 5 possible types:

* commercial
* office
* industrial
* logistics
* residential

**GROUP** (Optional)

Project that has group_id will be placed under that group. Otherwise, it's standalone node.

The coloring and placing is also handled by the group's `category` and `type`. Project's category and type are **ignored** when inserted to group.

**PLATFORM**

Platform is free text.

**YEAR**

If the value is `null`, it will be replaced with "In development".

Database Structure
===================

This is just a rough sketch of the DB relations (Based on Rails convention, feel free to adapt it to CodeIgniter).

    TABLE Locations
    - id

    TABLE Groups
    - id
    - location_id

    TABLE Projects
    - id
    - location_id
    - group_id (allow null)

Constant
==============

Inside `assets/js/constant.js` you will find the variable that stores constant like color and sizing.

**Note**: to fit client's requirement, don't change the canvas and graph sizing.

Using the JS
===============

Open `assets/js/data.js` and write the JSON call and assign it to variable named `data`.

If we follow the JSON format, there is no need to change the JS that handles the drawing.

The code automatically divides the circle based on number of locations and category availability.